﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Supermercado.Modelo
{
    class Produto
    {
        private int id;
        private String nome;
        private String categoria;
        private Double preco;

        public int _id { get { return id; } set { id = value; } }
        public String _nome { get { return nome; } set { nome = value; } }
        public String _categoria { get { return categoria; } set { categoria = value; } }
        public Double _preco { get { return preco; } set { preco = value; } }

        public Produto(int i, String n, String c, Double p)
        {
            this.id = i;
            this.nome = n;
            this.categoria = c;
            this.preco = p;
        }
    }
}
