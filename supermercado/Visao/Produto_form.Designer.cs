﻿namespace Supermercado
{
    partial class Produto_form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.campo_id = new System.Windows.Forms.TextBox();
            this.campo_nome = new System.Windows.Forms.TextBox();
            this.campo_categoria = new System.Windows.Forms.TextBox();
            this.campo_valor = new System.Windows.Forms.TextBox();
            this.btn_cadastrar = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.lv_Produto = new System.Windows.Forms.ListView();
            this.bnt_list = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.btn_remover = new System.Windows.Forms.Button();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // campo_id
            // 
            this.campo_id.Location = new System.Drawing.Point(44, 80);
            this.campo_id.Name = "campo_id";
            this.campo_id.Size = new System.Drawing.Size(72, 20);
            this.campo_id.TabIndex = 0;
            // 
            // campo_nome
            // 
            this.campo_nome.Location = new System.Drawing.Point(41, 132);
            this.campo_nome.Name = "campo_nome";
            this.campo_nome.Size = new System.Drawing.Size(335, 20);
            this.campo_nome.TabIndex = 1;
            // 
            // campo_categoria
            // 
            this.campo_categoria.Location = new System.Drawing.Point(41, 195);
            this.campo_categoria.Name = "campo_categoria";
            this.campo_categoria.Size = new System.Drawing.Size(151, 20);
            this.campo_categoria.TabIndex = 2;
            // 
            // campo_valor
            // 
            this.campo_valor.Location = new System.Drawing.Point(225, 195);
            this.campo_valor.Name = "campo_valor";
            this.campo_valor.Size = new System.Drawing.Size(151, 20);
            this.campo_valor.TabIndex = 3;
            // 
            // btn_cadastrar
            // 
            this.btn_cadastrar.Location = new System.Drawing.Point(225, 235);
            this.btn_cadastrar.Name = "btn_cadastrar";
            this.btn_cadastrar.Size = new System.Drawing.Size(151, 39);
            this.btn_cadastrar.TabIndex = 4;
            this.btn_cadastrar.Text = "Cadastrar";
            this.btn_cadastrar.UseVisualStyleBackColor = true;
            this.btn_cadastrar.Click += new System.EventHandler(this.btn_cadastrar_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(41, 64);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(16, 13);
            this.label1.TabIndex = 5;
            this.label1.Text = "Id";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(41, 116);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(44, 13);
            this.label2.TabIndex = 6;
            this.label2.Text = "Produto";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(41, 179);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 13);
            this.label3.TabIndex = 7;
            this.label3.Text = "Categoria";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(222, 179);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(31, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Valor";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label5.Location = new System.Drawing.Point(36, 31);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(90, 25);
            this.label5.TabIndex = 9;
            this.label5.Text = "Produtos";
            // 
            // lv_Produto
            // 
            this.lv_Produto.Location = new System.Drawing.Point(44, 290);
            this.lv_Produto.Name = "lv_Produto";
            this.lv_Produto.Size = new System.Drawing.Size(332, 169);
            this.lv_Produto.TabIndex = 10;
            this.lv_Produto.UseCompatibleStateImageBehavior = false;
            // 
            // bnt_list
            // 
            this.bnt_list.Location = new System.Drawing.Point(225, 483);
            this.bnt_list.Name = "bnt_list";
            this.bnt_list.Size = new System.Drawing.Size(151, 50);
            this.bnt_list.TabIndex = 11;
            this.bnt_list.Text = "Atualizar";
            this.bnt_list.UseVisualStyleBackColor = true;
            this.bnt_list.Click += new System.EventHandler(this.bnt_list_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 556);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(421, 22);
            this.statusStrip1.TabIndex = 12;
            this.statusStrip1.Text = "statusStrip1";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(145, 17);
            this.toolStripStatusLabel1.Text = "Nenhum item cadastrado!";
            // 
            // btn_remover
            // 
            this.btn_remover.Location = new System.Drawing.Point(44, 465);
            this.btn_remover.Name = "btn_remover";
            this.btn_remover.Size = new System.Drawing.Size(26, 23);
            this.btn_remover.TabIndex = 13;
            this.btn_remover.Text = "-";
            this.btn_remover.UseVisualStyleBackColor = true;
            this.btn_remover.Click += new System.EventHandler(this.btn_remover_Click);
            // 
            // Produto_form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(421, 578);
            this.Controls.Add(this.btn_remover);
            this.Controls.Add(this.statusStrip1);
            this.Controls.Add(this.bnt_list);
            this.Controls.Add(this.lv_Produto);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_cadastrar);
            this.Controls.Add(this.campo_valor);
            this.Controls.Add(this.campo_categoria);
            this.Controls.Add(this.campo_nome);
            this.Controls.Add(this.campo_id);
            this.Name = "Produto_form";
            this.Text = "Produto";
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox campo_id;
        private System.Windows.Forms.TextBox campo_nome;
        private System.Windows.Forms.TextBox campo_categoria;
        private System.Windows.Forms.TextBox campo_valor;
        private System.Windows.Forms.Button btn_cadastrar;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.ListView lv_Produto;
        private System.Windows.Forms.Button bnt_list;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.Button btn_remover;
    }
}