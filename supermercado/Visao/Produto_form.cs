﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Supermercado.Controle;
using Supermercado.Modelo;
using Newtonsoft.Json;

namespace Supermercado
{
    public partial class Produto_form : Form
    {
        Produto_controle produto;
        public Produto_form()
        {
            produto = new Produto_controle();
            InitializeComponent();
            inicializaListView();
        }

        private void btn_cadastrar_Click(object sender, EventArgs e)
        {

            if (campo_nome.Text != "" && campo_categoria.Text != "" && campo_valor.Text.ToString() != "")
            {

                int id = int.Parse(campo_id.Text);
                String nome = campo_nome.Text;
                String cat = campo_categoria.Text;
                //String checaDouble = campo_valor.Text;

                Double valor = Double.Parse(campo_valor.Text);


                //cadastra na list
                this.produto.add(id, nome, cat, valor);


                //Status no rodapé
                //toolStripStatusLabel1.Text = string.Format("Ultimo item cadastrado {0}", produto._listaProduto.ElementAt(id)._nome);
                //statusStrip1.Refresh();

                campo_id.Text = String.Empty;
                campo_nome.Text = String.Empty;
                campo_categoria.Text = String.Empty;
                campo_valor.Text = String.Empty;
            }
            else
            {
                MessageBox.Show("Precisa preencher todos os campos", "Erro",  MessageBoxButtons.OK, MessageBoxIcon.Hand);
            }
        }

        //ListView item = new ListView();
        ListViewItem item = new ListViewItem();
        

        private void inicializaListView()
        {
            this.lv_Produto.FullRowSelect = true; //  seleciona a linha inteira
            this.lv_Produto.GridLines = true; // ativa o guide lines
            this.lv_Produto.View = View.Details; // 

            ColumnHeader titulo_id = new ColumnHeader();
            ColumnHeader titulo_nome = new ColumnHeader();
            ColumnHeader titulo_categoria = new ColumnHeader();
            ColumnHeader titulo_valor = new ColumnHeader();

            titulo_id.Text = "ID";
            titulo_nome.Text = "Nome";
            titulo_categoria.Text = "Categoria";
            titulo_valor.Text = "Valor";

            this.lv_Produto.Columns.Add(titulo_id);
            this.lv_Produto.Columns.Add(titulo_nome);
            this.lv_Produto.Columns.Add(titulo_categoria);
            this.lv_Produto.Columns.Add(titulo_valor);

        }

        private void bnt_list_Click(object sender, EventArgs e)
        {
            if (lv_Produto.Items.Count > 0)
                lv_Produto.Items.Clear();

                int tam = 4;
                String[] produtoView = new String[tam];
                for (int i = 0; i < produto._listaProduto.Count; i++)
                {

                    String[] novoItem = new String[4];
                    novoItem[0] = produto._listaProduto.ElementAt(i)._id.ToString();
                    novoItem[1] = produto._listaProduto.ElementAt(i)._nome;
                    novoItem[2] = produto._listaProduto.ElementAt(i)._categoria;
                    novoItem[3] = produto._listaProduto.ElementAt(i)._preco.ToString();

                    ListViewItem item = new ListViewItem(novoItem);//lv_Produto.Items.Add(i.ToString());

                    lv_Produto.Items.Add(item);
                }
        }

        private void btn_remover_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < lv_Produto.Items.Count; i++)
            {
                if (this.lv_Produto.Items[i].Selected)
                {
                    lv_Produto.Items.RemoveAt(i);
                    produto._listaProduto.RemoveAt(i);
                }
            }
        }

    }
}
