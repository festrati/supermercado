﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Supermercado.Modelo;
using Newtonsoft.Json;

namespace Supermercado.Controle
{
    class Produto_controle
    {
        public List<Produto> _listaProduto{ get; set; }
        
        public Produto_controle()
        {
            this._listaProduto = new List<Produto>();
        }

        public void add(int id, String nome, String cat, Double preco)
        {
            Produto p = new Produto(id, nome, cat, preco);
            this._listaProduto.Add(p);
        }

        public Produto buscaProduto(int id)
        {
            for (int i = 0; i < this._listaProduto.Count; i++)
            {
                if (this._listaProduto.ElementAt(i)._id == id)
                {
                    this._listaProduto.ElementAt(i);
                }
            }
            return null;
        }

        public Produto removeProduto(int id)
        {
            for (int i = 0; i < this._listaProduto.Count; i++)
            {
                if (this._listaProduto.ElementAt(i)._id == id)
                {
                    this._listaProduto.RemoveAt(i);
                }
            }
            return null;
        }


        //Checa se o campo está preechido com numeros - double
        public Boolean checaNumero(String num)
        {
            Double checaValor;
            Boolean valor = Double.TryParse(num, out checaValor);
            if (valor)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
